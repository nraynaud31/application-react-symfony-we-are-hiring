import React from 'react'
import Header from '../Header/Header';
import './signup.css';

class Sign_Up extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showForm: true,
            email: '',
            password: '',
            confirm_password: '',
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(event) {

        this.setState({
            [event.target.name]: event.target.value
        })
        console.log(this.state);
    }

    handleSubmit(event) {
        event.preventDefault();

        const apiUrl = `/api/register`;

        fetch(apiUrl, {
            headers: { 'Content-Type': 'application/json' },
            method: 'POST',
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password,
            })
        })
            .then(response => response.json())
            .then(datas => {
                console.log("datas:", datas);
                this.props.change("Home")
            })
    }

    render() {
        return (
            <div className='Signup'>
                <Header titre={"Sign up"} />
                {this.state.showForm &&
                    <form className="form" onSubmit={this.handleSubmit}>
                        <label><strong>Email</strong></label><br></br>
                        <input type="email" className='email' required name="email" onChange={this.handleChange} value={this.state.email}></input><br></br>
                        <div className='password_sec'>
                            <label><strong>Password</strong></label><br></br>
                            <input type="password" className='password' required name="password" onChange={this.handleChange} value={this.state.password}></input><br></br>
                        </div>
                        <div className='confirm_password_sec'>
                            <label><strong>Confirm password</strong></label><br></br>
                            <input type="password" className='confirm_password' required name="confirm_password" onChange={this.handleChange} value={this.state.confirm_password}></input><br></br>
                        </div>
                        <a className='signup_submit'><input className='signup_btn' type="submit" value="Sign Up" /></a>
                        <p className='purpose'>You already have an account yet? <br></br><a className='login_account' onClick={() => this.props.change("Login")}>Sign in</a></p>
                    </form>}
            </div>
        );
    }
}

export default Sign_Up