import React from 'react'
import Header from '../Header/Header';
import './login.css';

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value })
    }

    handleSubmit(event) {
        this.props.change("Home");
    }

    render() {
        return (
            <div className='Login'>
                <Header titre="Sign in" />
                <form className='login_form' onSubmit={() => (this.props.callback('Home'))}>
                    <label><strong>Email</strong></label><br></br>
                    <input type="email" required className='email' onChange={this.handleChange}></input><br></br>
                    <div className='password'>
                        <label><strong>Password</strong></label><br></br>
                        <input type="password" required className='password_input' onChange={this.handleChange}></input><br></br>
                    </div>
                    <span onClick={() => this.props.change('Home')}><input className='login_send' type="button" value="Sign in" /></span>
                    <p className='purpose'>Don't have an account yet? <br></br><span className='create_account' onClick={() => this.props.change("SignUp")}>Create an account</span></p>

                </form>
            </div>
        )
    }
}

export default Login