import React from 'react'
import "./footer.css"
export default class Footer extends React.Component {

    styles = {
        backgroundColor: '#F2F2F2',
        position: 'absolute',
        sticky: 'bottom',
        bottom: '0',
        justifyContent: 'center',
    }

    render() {
        return (
            <footer>
                <div>
                    <h4>WE ARE HIRING</h4>
                    <hr className='hr_footer'></hr>
                    <p>© 2022 - We Are Hiring</p>
                </div>
            </footer>
        )
    }
}