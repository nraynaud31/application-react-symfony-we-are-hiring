export const MenuItems = [
    {
        title: 'Home',
        url: '/',
        cName: 'nav-links',

    },
    {
        title: 'Offers',
        url: '/offers',
        cName: 'nav-links',

    },
    {
        title: 'Sign Up',
        url: '/signup',
        cName: 'nav-links',

    },
    {
        title: 'Login',
        url: '/login',
        cName: 'nav-links',

    },
    {
        title: 'Contact',
        url: '/contact',
        cName: 'nav-links',

    }

]