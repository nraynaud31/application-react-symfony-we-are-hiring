import React, { Component } from 'react';

import './navbar.css';
class Navbar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            clicked: false,
        };
        this.handleClick = this.handleClick.bind(this)
        this.change = this.change.bind(this)
    }

    handleClick() {
        this.setState({ clicked: !this.state.clicked })
    }
    change(input) {

        this.props.change(input)
    }

    render() {
        return (
            <nav className='NavbarItems' >
                <h2 className='navbar-logo'>WE ARE HIRING</h2>
                <div className='menu-icon' onClick={this.handleClick}>
                    <i className={this.state.clicked ? 'fas fa-times' : 'fas fa-bars'}></i>
                </div>
                <ul className={this.state.clicked ? 'nav-menu active' : 'nav-menu'}>


                    <li>
                        <span className='cName' onClick={() => { this.change("Home") }}>Home</span>
                    </li>
                    <li>
                        <span className='cName' onClick={() => { this.change("Offers") }}>Offers</span>
                    </li>
                    <li>
                    <span className='cName' onClick={() => { this.change("My_Applications") }}>My Applications</span>
                    </li>
                    <li>
                        <span className='cName' onClick={() => { this.change("Favorites") }}>Favorites</span>
                    </li>
                    <li>
                        <span className='cName' onClick={() => { this.change("Login") }}>Login</span>
                    </li>
                    <li>
                        <span className='cName' onClick={() => { this.change("Contact") }}>Contact</span>
                    </li>
                    <li>
                        <span className='cName' onClick={() => { this.change("SignUp") }}>Sign Up</span>
                    </li>
                </ul>

            </nav>

        );
    }



}


export default Navbar;