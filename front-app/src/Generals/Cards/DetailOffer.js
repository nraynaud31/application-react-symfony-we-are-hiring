import React from 'react';
import ApplyForm from '../../Offers/Apply/ApplyForm';

class DetailOffer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            offerId: 0,
            page: 'DetailOffer',
            clicked: false,
        }
        this.change = this.change.bind(this)
    }

    change(input) {
        this.setState({ page: input })
    }

    // handleOffer() {
    //     const { id } = this.props.detailOffer;
    // }

    // componentDidMount() {
    //     console.log(this.props.detailOffer);
    //     fetch("http://localhost:3004/{offres.id}")
    //         .then((response) => response.json())
    //         .then(responses => {
    //             this.setState({
    //                 data: responses,
    //                 offerId: this.state.data.id
    //             })
    //         }).catch((err) => { console.log(err); })
    // }

    render() {
        if (this.state.page === 'DetailOffer') {
            const {
                id, type, job_name, city, created_at, description
            } = this.props.detailOffer;
            return (
                <div className='single-offer' key={id}>
                    <div className='offer_sec_1'>
                        <div className='Job'>
                            <p><strong>{job_name}</strong> </p>
                        </div>
                        <div className='loc'>
                            <p className='type'><strong>{type}</strong></p>
                            <p><strong>{city}</strong></p>
                        </div>
                    </div>
                    <p className='desc'>{description}</p>

                    <p className='publish_date'><strong>Published on {created_at}</strong></p>
                    <button id='button' onClick={() => this.change('ApplyForm')}>Apply</button>
                </div>
            )
        }
        if (this.state.page === 'ApplyForm') {
            return (
                <ApplyForm offerTitle={this.props.detailOffer.title}
                offerId={this.props.detailOffer.id}/>
            )
        }
    }
}

export default DetailOffer;