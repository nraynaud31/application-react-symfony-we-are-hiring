import React from 'react';
import "./cards.css";

export default class Cards extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            detailOffer: undefined
        }
        this.handleOfferById = this.handleOfferById.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleOfferById(id) {
        const offer = this.state.data.filter(e => e.id === id)[0]
        if (offer) {
            this.setState({
                detailOffer: offer,
            })
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.callback(this.state.detailOffer);
    }

    componentDidMount() {
        fetch(`/api/offers`)
            .then((data) => {
                return data.json()
            }).then(response => {
                console.log(response)
                this.setState({ data: response })

            })
    }

    render() {
        return (
            <div className='Cards'>
                {this.state.detailOffer === undefined && this.state.data.map(item => {
                    return (
                        <div key={item.id} className="card-body">
                            <div className="created-at">{item.created_at}</div>
                            <div className='title'><strong>{item.title}</strong></div>
                            <div className="description">{item.description}</div>
                            <div className="last">
                                <div className="adress">{item.city} - {item.type}</div>
                                <span className='see_more' onClick={() => { this.props.callback(item); this.props.change("detail") }}>See more {">"}</span>
                            </div>
                        </div>
                    )
                })}
                {/* {this.state.detailOffer !== undefined && <DetailOffer detailOffer={this.state.detailOffer}/>} */}
            </div>
        )
    }
}
