import React from "react";
import './contact_form.css';

export default class ContactForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showForm: true,
            email: '',
            message: '',
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
        console.log(this.state)
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({ showForm: false })
        const url = `/api/contact`;

        fetch(url, {
            headers: { 'Content-Type': 'application/json' },
            method: 'POST',
            body: JSON.stringify({
                email: this.state.email,
                message: this.state.message
            }),
        })
            .then(response => response.json())
            .then(datas => {
                console.log("datas:", datas);
            })
    }


    render() {
        return (
            <div className="Contact_Form">
                <header className="contact_header">
                    <h1 className="headerTitle">Contact us</h1>

                </header>
                {this.state.showForm && <form onSubmit={this.handleSubmit}>
                    <label className="contact_label"><strong>Your email address</strong></label><br></br>
                    <input className="contact_email" type="email" name="email" required value={this.state.email} onChange={this.handleChange}></input><br></br>
                    <div className="msg">
                        <label className="contact_label"><strong>Your message</strong></label><br></br>
                        <input type="text" className="contact_msg" name="message" required value={this.state.message} onChange={this.handleChange}></input><br></br>
                    </div>
                    <button className="contact_send" type="submit" method="POST">Send</button>
                </form>}

                {
                    !this.state.showForm && <div>
                        <h1>Thanks!</h1>
                        <p><strong>Your application message has been sent!</strong></p>
                        <p>We thank you for your application.</p>
                        <br></br>
                        <p>We will respond as soon as possible.</p>
                        <br></br>
                        <p>See you soon</p>
                    </div>
                }

            </div>
        )
    }
}
