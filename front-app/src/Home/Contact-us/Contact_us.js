import React from "react";
import "./contact_us.css"
export default class ContactHome extends React.Component {

    render() {
        return (
            <div className="contact_us">
                <div>
                    <p><strong>Ready to get started?</strong></p>
                    <p>Contact us</p>
                </div>
                <div><button className="contact_btn">Contact us</button></div>


            </div>
        )
    }
}