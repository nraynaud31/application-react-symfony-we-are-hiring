import React from "react";
import Cards from "../Generals/Cards/Cards";
import ContactHome from "./Contact-us/Contact_us";
import DetailOffer from '../Generals/Cards/DetailOffer';
import Offers from "../Offers/Offers";
import Header from "../Generals/Header/Header";
import "../App.css"
import "../Home/Home.css"


export default class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            page: '',
            data: [],
            clicked: false,
        }
        this.change = this.change.bind(this)
        this.callback = this.callback.bind(this)
    }

    change(input) {
        this.setState({ page: input })
        console.log(input);
    }

    callback(params) {
        this.setState({ data: params })
        console.log("callback:", params);
    }

    render() {
        if (this.state.page === '') {
            return (
                <div className="Home">
                    <header className="home_header">
                        <div className='header-container'>
                            <h1>Finding a job has never been easier</h1>
                            <h3 id="desc">We hiring allows you to find the best job offers on the market and apply quickly and securely.</h3>
                            <span id="button" onClick={() => this.props.change("Offers")}>See all offers</span>
                        </div>
                    </header>
                    <div className="about">
                        <h2>About us</h2>
                        <h3>Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit, sed to eiusmod temporem
                            ipsum dolor sit amet, consectetur adipiscing
                            elit, sed do eiusmod tempor</h3>
                    </div>
                    <div className="last-offers">
                        <h2 id="title">Last offers</h2>
                        <div className="cards_list">
                            <Cards callback={this.callback} change={this.change} />
                        </div>
                        
                    </div>
                    <div className="contact_us">
                        <div>
                            <h3 className="title"><strong>Ready to get started?</strong></h3>
                            <p id="contact_us_p">Sign up or Contact us</p>
                        </div>
                        <div id="contact_btns">
                            <div>
                                <button id="contact_signup_button" onClick={() => this.props.change("SignUp")}>Sign up</button>
                            </div>
                            <div>
                                <button className="contact_btn" onClick={() => this.props.change('Contact')}>Contact us</button>

                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        if (this.state.page === 'detail') {
            console.log("avec value");
            return (
                <div className="Home_detail">
                    <Header titre={this.state.data.title} />
                    <DetailOffer detailOffer={this.state.data} change={this.change} />
                </div>
            )
        }
        else if (this.state.page === 'Offers') {
            return (
                <Offers />
            )
        }
        else if (this.state.page === 'Contact_home') {
            return (
                <ContactHome />
            )
        }
    }

}
