import React from "react";
import './my_application.css';
export default class MyApplications extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            candidatures: [],
            offres: []
        }
    }

    componentDidMount() {
        this.fetchOffersApplied();
    }
    fetchOffersApplied() {
        fetch('http://localhost:3004/candidatures')
            .then(response => response.json())
            .then(json => this.setState({ candidatures: json }))
            .then(() => fetch('http://localhost:3004/offres')
                .then(response => response.json())
                .then(json => { this.setState({ offres: json }) })
            )
    }

    render() {
        console.log('candidatures: ', this.state.candidatures)
        console.log('offres: ', this.state.offres)

        // if (this.state.offres.length === 0 || this.state.candidatures === 0){
        //     return (<div></div>)
        // }

        const getApplyId = this.state.candidatures.map(element => element.offerId);
        console.log('candidaturesID: ', getApplyId)

        const applications = this.state.offres.filter(offres => getApplyId.includes(offres.id));

        console.log('offres2 :', this.state.offres)
        console.log('application :', this.state.applications)

        // const {
        //     id, title, type, job_name, city, created_at, description
        // } = this.state.applications;
        // console.log(id, title, type, job_name, city, created_at, description);
        return (
            <div className="appliedCards" >
                {applications.map(item => {
                    return <div key={item.id} className="card-body">
                        <div className='appliedOn'>applied on{item.created_at}</div>
                        <div className='title'>{item.title}</div>
                        <div className="city">{item.city}</div>
                        <div className="type">{item.type}</div>
                    </div>
                })
                }
            </div>
        )
    }
}


