import React from "react";
import Header from "../Generals/Header/Header";
import DetailOffer from "../Generals/Cards/DetailOffer";
import Pagination from "./Pagination";
import '../Offers/Offers.css';

class Offers extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            page: '',
            data: [],
            pages: 1,
            totalPages: 0,
            recordsPerPage: 9,
            detailOffer: undefined
        }
        this.change = this.change.bind(this)
        this.callback = this.callback.bind(this)
    }

    change(input) {
        this.setState({ page: input })
    }

    callback(params) {
        this.setState({ data: params })
    }

    componentDidMount() {
        this.getOffers();
    }
    getOffers() {
        fetch(`/api/offers`)
            .then((data) => {
                return data.json()
            }).then(response => {
                console.log(response)

                const totalPages = Math.floor(response / this.state.recordsPerPage)
                this.setState({ data: response, totalPages: totalPages })
            })
    }


    onChangeRecordsPerPage(event) {
        this.setState({ recordsPerPage: parseInt(event.target.value) }, () => {
            this.getOffers()
        })
    }


    onPageChanged(pages) {
        this.setState({ pages: pages }, () => {
            this.getOffers()
        })
    }

    render() {
        if (this.state.page === '') {
            return (
                <div className="Offers">

                    <Header titre={"Offers"} />
                    <div className="cards-container">
                        <div className="card-lists">

                            {this.state.detailOffer === undefined && this.state.data.map((item, index) => {
                                return (
                                    <div className="cards_details" key={index}>
                                        <div className="created-at">{item.created_at}</div>
                                        <div className="job-name"><strong>{item.title}</strong></div>
                                        <div className="description">{item.description}</div>
                                        <div className="last">
                                            <div className="adress">{item.city} - {item.type}</div>
                                            <span onClick={() => { this.callback(item); this.change("detail") }}> See more {">"}</span>
                                        </div>
                                    </div>


                                )
                            })
                            }
                            {
                                this.state.data.length === 0 ?
                                    <Pagination totalPages={this.state.totalPages} currentPage={this.state.pages} maxVisibleButtons={3} onPageChanged={(e) => this.onPageChanged(e)} /> : <div />

                            }
                        </div>
                    </div>
                    {
                        this.state.data.length > 0 ?
                            <Pagination totalPages={this.state.totalPages} currentPage={this.state.pages} maxVisibleButtons={3} onPageChanged={(e) => this.onPageChanged(e)} /> : <div />
                    }

                </div>

            )
        }

        if (this.state.page === 'detail') {
            return (
                <div className="Offer">
                    <Header titre={this.state.data.title} />
                    <DetailOffer detailOffer={this.state.data} change={this.change} />
                </div>
            )
        }

    }

}

export default Offers;