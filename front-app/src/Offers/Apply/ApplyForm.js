import React from 'react';

export default class ApplyForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showForm: true,
            title: '',
            firstname: '',
            lastname: '',
            email: '',
            phone: '',
            adress: '',
            building: '',
            postcode: '',
            city: '',
            profil: '',
            gitProfil: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    // handleChange(event) {
    //     const value = event.target.value;
    //     this.setState({
    //         ...this.state,
    //         [event.target.name]: value
    //     });


    handleChange(event) {

        this.setState({
            [event.target.name]: event.target.value
        })
        console.log(this.state);
        // this.setState({ firstname: event.target.firstname });
        // this.setState({ lastname: event.target.lastname });
        // this.setState({ email: event.target.email });
        // this.setState({ phone: event.target.phone });
        // this.setState({ adress: event.target.adress });
        // this.setState({ building: event.target.building });
        // this.setState({ postcode: event.target.postcode });
        // this.setState({ city: event.target.city });
        // this.setState({ profil: event.target.profil });
        // this.setState({ gitProfil: event.target.gitProfil });
    }

    handleSubmit(event) {
        this.setState({ showForm: false })
        console.log(this.state);
        event.preventDefault();

        const apiUrl = `http://localhost:3004/candidatures`;

        fetch(apiUrl, {
            headers: { 'Content-Type': 'application/json' },
            method: 'POST',
            body: JSON.stringify({
                firstname: this.state.firstname,
                lastname: this.state.lastname,
                email: this.state.email,
                phone: this.state.phone,
                adress: this.state.adress,
                building: this.state.building,
                postcode: this.state.postcode,
                city: this.state.city,
                profil: this.state.profil,
                gitProfil: this.state.gitProfil,
                offerId: this.props.offerId,
            })
        })
            .then(response => response.json())
            .then(datas => {
                console.log("datas:", datas);
            })
    }

    render() {
        return (
            <div className="formApply">
                <div className='apply_head'>
                    <h3 className='apply_title'><strong>Apply to</strong></h3>
                    <h2 className='offer_title_apply'>{this.props.offerTitle}</h2>
                </div>
                {this.state.showForm && <form className="form" onSubmit={this.handleSubmit}>
                    <div className='firstname_div'>
                        <label><strong>Your firstname <span className='red_star'>*</span></strong></label><br></br>
                        <input type="text" name="firstname" required="required" value={this.state.firstname} onChange={this.handleChange} placeholder="Edy" /><br></br>
                    </div>
                    <div className='lastname_div'>
                        <label><strong>Your lastname <span className='red_star'>*</span></strong></label><br></br>
                        <input type="text" name="lastname" required="required" value={this.state.lastname} onChange={this.handleChange} placeholder="Murphy" /><br></br>
                    </div>
                    <div className='email_div'>
                        <label><strong>Your email adress <span className='red_star'>*</span></strong></label><br></br>
                        <input type="email" name="email" required="required" value={this.state.email} onChange={this.handleChange} placeholder="edy.murphy@gmail.com" /><br></br>
                    </div>
                    <div className='phone_num_div'>
                        <label><strong>Your phone number adress</strong></label><br></br>
                        <input type="text" name="phone" placeholder="+ 33 6 13  78 09 78" value={this.state.phone} onChange={this.handleChange} /><br></br>
                    </div>
                    <div className='address_div'>
                        <label><strong>Your adress</strong></label><br></br>
                        <input type="text" name="adress" placeholder="775 alabama street" value={this.state.adress} onChange={this.handleChange} /><br></br>
                        <input type="text" name="building" placeholder="Block C" value={this.state.building} onChange={this.handleChange} />
                        <div className='address_2'>
                            <div className='address_cp'>
                                <input type="number" name="postcode" placeholder="31 000" value={this.state.postcode} onChange={this.handleChange} />
                            </div>
                            <div className='address_city'>
                                <input type="text" name="city" placeholder="Toulouse" value={this.state.city} onChange={this.handleChange} /><br></br>
                            </div>
                        </div>
                    </div>
                    <div className='linkedin_div'>
                        <label><strong>Your linkedIn profil</strong></label><br></br>
                        <input type="url" name="profil" placeholder="htttps://..." value={this.state.profil} onChange={this.handleChange} /><br></br>
                    </div>
                    <div className='git_div'>
                        <label><strong>Your GitHub/GitLab profil</strong></label><br></br>
                        <input type="url" name="gitProfil" placeholder="htttps://..." value={this.state.gitProfil} onChange={this.handleChange} /><br></br>
                    </div>
                    <button className='apply_btn' type="submit" method="POST">Apply</button>
                </form>}

                {
                    !this.state.showForm && <div>
                        <h1>Thanks!</h1>
                        <p><strong>Your application to "{this.props.offerTitle}" offer has been sent!</strong></p>
                        <p>We thank you for your application.</p>
                        <br></br>
                        <p>We will respond as soon as possible.</p>
                        <br></br>
                        <p>See you soon</p>
                    </div>
                }
            </div>
        );
    }
}