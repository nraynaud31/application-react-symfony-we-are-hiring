import React from 'react';
import './App.css';
import ContactForm from './Contact/ContactForm';
import Navbar from './Generals/Navbar/Navbar';
import Home from './Home/Home'
import Offers from './Offers/Offers';
import Footer from './Generals/Footer/Footer';
import Header from './Generals/Header/Header';
import { useState } from 'react'
import SignUp from './Generals/Auth/SignUp';
import Login from './Generals/Auth/Login';
import MyApplications from './Offers/My_Applications';
import Favorites from './Offers/Favorites';

function App() {

  let [page, setPage] = useState('Home');
  let [login, setlogin] = useState(false)

  function change(page) {
    setPage(page)
  }

  function belogin(logged) {
    setlogin(logged)
  }

  if (page === 'Home') {

    return (
      <div className='app'>
        <Navbar change={change} login={login} belogin={belogin} />
        <Home change={change} />
        <Footer />
      </div>
    )

  }
  if (page === 'Offers') {
    return (
      <div className='app'>
        <Navbar change={change} login={login} belogin={belogin} />
        <Offers />
        <Footer />
      </div>
    )
  }

  if (page === 'Contact') {
    return (
      <div className='app'>
        <Navbar change={change} />
        <ContactForm change={change} />
        <Footer />
      </div>
    )
  }

  if (page === 'Login' && login === false) {
    return (
      <div className='app'>
        <Navbar change={change} login={login} belogin={belogin} />
        <Login change={change} />
        <Footer />
      </div>
    )
  }

  if (page === 'SignUp' && login === false) {
    return (
      <div className='app'>
        <Navbar change={change} login={login} belogin={belogin} />
        <SignUp change={change} />
        <Footer />
      </div>
    )
  }

  if (page === 'My_Applications') {
    return (
      <div>
        <Navbar change={change} login={login} belogin={belogin} />
        <Header titre={"My applications"} />
        <MyApplications change={change} />
        <Footer />
      </div>
    )
  }

  if (page === 'Favorites' && login === true) {
    return (
      <div>
        <Navbar change={change} login={login} belogin={belogin} />
        <Favorites change={change} />
        <Footer />
      </div>
    )
  }

}



export default App;