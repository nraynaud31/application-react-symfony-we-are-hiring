<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\Persistence\ManagerRegistry;
class RegistrationController extends AbstractController
{
#[Route(path: '/api/register', name: 'app_register')]
public function register(ManagerRegistry $doctrine, Request $request): Response
{
    $entityManager = $doctrine->getManager();    
    
    $username = $request->request->get('email');
    $password = $request->request->get('password');
    $confirmPassword = $request->request->get('confirm_password');

    if($password != $confirmPassword){
        return new Response('account creation failed');
    }
    
    $repository = $doctrine->getRepository(User::class);
    $user = $repository->findOneBy(['username' => $username]);

    if($user != null){
        return new Response('username already exists');
    }

    $newUser = new User();

    $newUser->setUsername($username);
    $newUser->setPassword($password);

    $entityManager->persist($newUser);
    $entityManager->flush();
    return new Response ('account created');
}
}

