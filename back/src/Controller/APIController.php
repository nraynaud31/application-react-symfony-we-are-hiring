<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Security;
use App\Entity\User;

/**
 * @Route("/api")
 */
class APIController extends AbstractController
{
    

    /**
     * @Route("/application/{offer_id}")
     */
    public function protectedAction($offer_id)
    {
        return $this->json(["message" => $offer_id."The API successfully validated your access token."], Response::HTTP_OK);
    }

    /**
     * @Route("/contact")
     */
    public function adminAction()
    {
        return $this->json(["message" => "The API successfully recognized you as an admin."], Response::HTTP_OK);
    }
    


    /**
     * Get Token of user
     * @param Security $security
     * @param JWTTokenManagerInterface $JWTManager
     * @return JsonResponse
     */
    public function getTokenUser(Security $security, JWTTokenManagerInterface $JWTManager)
    {
        //dd('ici');
        $user = new User('bob', 'password');
        return $this->json(['token' => $JWTManager->create($user)]);
    }
}
