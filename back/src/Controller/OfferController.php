<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Offers;


class OfferController extends AbstractController
{

    private $managerRegistry;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    #[Route('/api/offers/{id}', name: 'offers_id')]
    public function show(int $id, ManagerRegistry $doctrine): Response
    {

        $offer = $doctrine->getRepository(Offers::class)->find($id);
        $res= array(
            'id' => $offer->getId(),
            'title' => $offer->getTitle(),
            'type' => $offer->getType(),
            'function' => $offer->getFunction(),
            'description' => $offer->getDescription(),
            'city' => $offer->getCity(),
            'created_at' => $offer->getCreatedAt(),
        );

        return $this->json(["details" => $res], Response::HTTP_OK);
    }

    #[Route('/api/offers', name: 'offers')]
    public function showAll(ManagerRegistry $doctrine): Response
    {

        $offer = $doctrine->getRepository(Offers::class)->findAll();
        foreach($offer as $item){
            $res[]= array(
            'id' => $item->getId(),
            'title' => $item->getTitle(),
            'type' => $item->getType(),
            'function' => $item->getFunction(),
            'description' => $item->getDescription(),
            'city' => $item->getCity(),
            'created_at' => $item->getCreatedAt(),
        );
        }
        

        return $this->json(["details" => $res], Response::HTTP_OK);
    }
}
